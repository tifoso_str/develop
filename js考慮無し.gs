function scraping() {
 
  const URL = 'https://www.eb.pref.okinawa.jp/kassui/';//沖縄県企業局のダム貯水率　           
  
  var response = UrlFetchApp.fetch(URL);
  var html = response.getContentText('UTF-8');
  
  var myRegexp = /<title>([\s\S]*?)<\/title>/;
  var title = html.match(myRegexp);
  Logger.log("タイトル：%s",title);
 
  var myRegexp = /id=\"chosui_hiduke\">([\s\S]*?)<\/span>/;
  var day = html.match(myRegexp);
  Logger.log("日付：%s",day);
  
  var myRegexp = /id=\"ritsu_today4\">([\s\S]*?)<\/td>/;
  var day = html.match(myRegexp);
  Logger.log("貯水率：%s",day);
}

function test() {
  
  var str= "<h3>ダム貯水率状況</h3><p><span id=point_diff>　</span> ポイントとなっています。</p><h3>ダム</h3>";
 
  var reg = /<h3>([\s\S]*?)<\/h3>/;

  Logger.log(str.match(reg));
}


